all:
	gcc -W -Wall -Werror ./main.c -o tkabreak
	
run:
	gcc -W -Wall -Werror ./main.c -o tkabreak
	./tkabreak
	
debug:
	gcc -W -Wall -Werror ./main.c -g3 -o main
