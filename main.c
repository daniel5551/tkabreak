#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

void sounds();
void timer(int worktime, int breaktime);
void productivity();
void breath();

int main(){
    int menu;
    puts("-----Menu----\n 1.Play Sounds\n 2.Productivity Timer\n 3.Breath\n-------------");
    scanf("%d", &menu);
    if(menu == 1){
        sounds();
    }else if(menu == 2){
        productivity();
    }else if(menu == 3){
        puts("Work in process!");
        breath();
    }else{
        puts("Wrong Argument!");
        return 0;
    }
}

void breath(){
    puts("Breath!");
}

void sounds(){
    int soundid;
	char menu;
	char quit_command = 'q';
	pid_t pid;
        puts("-----Menu----\n 1.Costal\n 2.Crickets\n 3.Forest\n 4.Ocean Surf\n 5.Rain Leaves\n 6.Waterfall\n-------------");
        scanf("%d", &soundid);
        pid = fork();
        if(pid == 0){
            if(soundid == 1){
                puts("Coastal\n");
                system("ogg123 -q -r ./ambient/Coastal.ogg &");
                puts("Type q to quit");
                goto MENU;
            }else if(soundid == 2){
                puts("Crickets\n");
                system("ogg123 -q -r ./ambient/Crickets.ogg &");
                puts("Type q to quit");
                goto MENU;
            }else if(soundid == 3){
                puts("Forest");
                system("ogg123 -q -r ./ambient/Forest.ogg &");
                goto MENU;
            }else if(soundid == 4){
                puts("Ocean Surf\n");
                system("ogg123 -q -r ./ambient/Ocean-Surf.ogg &");
                puts("Type q to quit");
                goto MENU;
            }else if(soundid == 5){
                puts("Rain Leaves\n");
                system("ogg123 -q -r ./ambient/Rain-Leaves.ogg &");
                puts("Type q to quit");
                goto MENU;
            }else if(soundid == 6){
                puts("Waterfall\n");
                system("ogg123 -q -r ./ambient/Waterfall.ogg &");
                puts("Type q to quit");
                goto MENU;
            }else{
                puts("Wrong argument!");
                sounds();
            }
        }
	MENU:
	    menu = getchar();
	    if(menu == quit_command){
            kill(0, SIGKILL);
	    }
	    else{
            goto MENU;
	    }
    }

void timer(int worktime, int breaktime){
    char menu;
    char ready_command = 'r';
    char quit_command = 'q';
    int status;
    sleep(worktime * 60);
    puts("It's time to take a break!");
    system("mpg123 -q ./Alarm-Clock.mp3");
    puts("Type r, when you being ready to take a break, or q to quit");
    status = 1;
    while(status){
        menu = getchar();
        if(menu == ready_command){
             sleep(breaktime * 60);
             puts("It's work time!");
             system("mpg123 -q ./Alarm-Clock.mp3");
             puts("Type r, when you being ready to work, or q to quit");
             while(menu != ready_command || menu != quit_command){
                menu = getchar();
                if(menu == ready_command){
                    timer(worktime, breaktime);
                }
                else if(menu == quit_command){
                    exit(0);
                }
             }
         }
         else if(menu == quit_command){
            status = 0;
            exit(0);
        }
    }
}

void productivity(){
    int menu, breakTime, workTime;
    puts("----Menu----\n1. 60 minuites Work, 15 minuites break\n2. 25 minuites work, 5 minuite break\n3. 45 minuites work, 10 minuites break\n4. Custom");
    scanf("%d", &menu);
    if(menu == 1){
        timer(60, 15);
    }
    else if(menu == 2){
        timer(25, 5);
    }
    else if(menu == 3){
	timer(45, 10);
    }
    else if(menu == 4){
        puts("Enter Working time in minuites: ");
        scanf("%d", &workTime);
        puts("Enter Break time in minuites: ");
        scanf("%d", &breakTime);
        timer(workTime, breakTime);
    }
    else{
        productivity();
    }
}
