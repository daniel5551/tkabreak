import os, subprocess, random
from tkinter import *
from tkinter import ttk

soundlist = ["ocean-surf", "crickets", "rain-leaves", "waterfall", "coastal", "forest"] # List of possible sounds
status = False # Is play function enabled

def playrandom(): # throw random sound and play it with play() function
    global proc, soundlist, status
    if status == False:
        sound = random.choice(soundlist)
        play(sound)
    else:
        pass

def playnone(): # if nothing is played. quit, if not, stop playing
    global proc, status
    if status == True:
        proc.kill()
        status = False
    else:
        root.destroy()
        
def play(sound): # playing sound with ogg123
    global proc, status
    if status == False:
        proc = subprocess.Popen(["/usr/bin/ogg123", "-q", "-r", "./ambient/" + sound + ".ogg"])
        status = True
    else:
        pass
    
# TKinter initialization    
root = Tk()
frm = ttk.Frame(root, padding=10)
frm.grid()

# Images initialization
noneimg = PhotoImage(file='img/none.png')
randomimg = PhotoImage(file='img/random.png')
oceanimg =  PhotoImage(file='img/ocean.png')
cricketsimg =  PhotoImage(file='img/crickets.png')
rainimg =  PhotoImage(file='img/rain.png')
waterfallimg = PhotoImage(file='img/waterfall.png')
coastalimg = PhotoImage(file='img/coastal.png')
forestimg =  PhotoImage(file='img/forest.png')

# Buttons initialization
Button(root,
    image = noneimg,
    command=playnone).grid(column=1, row=1)
Button(root,
    image = randomimg,
    command=playrandom).grid(column=2, row=1)
Button(root,
    image = oceanimg,
    command=lambda sound="ocean-surf": play(sound)).grid(column=3, row=1)
Button(root,
    image = cricketsimg,
    command=lambda sound="crickets": play(sound)).grid(column=4, row=1)
Button(root,
    image = rainimg,
    command=lambda sound="rain-leaves": play(sound)).grid(column=5, row=1)
Button(root,
    image = waterfallimg,
    command=lambda sound="waterfall": play(sound)).grid(column=6, row=1)
Button(root,
    image = coastalimg,
    command=lambda sound="coastal": play(sound)).grid(column=7, row=1)
Button(root,
    image = forestimg,
    command=lambda sound="forest": play(sound)).grid(column=8, row=1)

root.mainloop()
