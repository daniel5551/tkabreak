# tkabreak - Universall app for relax and balanced work.

tkabreak - it's console-based program, written on C, and based on idea of Calm app.

## Features

- Set of nature sounds and preset's.
- Productivity timer.
- Improve you breath with Breathing Timer. (WIP)

## Sources

- Background Sounds Taken From Bejeweled 3 VideoGame Copy.
- [Alarm Clock (ACCIDENTS & DISASTERS,FIRE)](https://free-sound-effects.net/alarm)